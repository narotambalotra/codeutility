package com.scb.instrumentdata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.scb.instrumentdata.messaging.InstrumentListenerImpl;
import com.scb.instrumentdata.messaging.ThreadSafePublisher;
import com.scb.instrumentdata.model.Instrument;
import com.scb.instrumentdata.process.InstrumentProcessor;
import com.scb.instrumentdata.util.ApplicationHelper;



@SpringBootApplication
public class InstrumentDataApp {
	private static final Logger logger = LoggerFactory.getLogger(InstrumentDataApp.class);
	public static void main(String[] args) {
		SpringApplication app =new SpringApplication(InstrumentDataApp.class);
		app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
        case1();
        case2();
		case3();
	}
	

	private static void case1() {
		InstrumentProcessor processor = new InstrumentProcessor();

		ThreadSafePublisher publisher = new ThreadSafePublisher(ApplicationHelper.LME_CODE);
		InstrumentListenerImpl listener = new InstrumentListenerImpl(processor);
		publisher.registerListener(listener);

		Instrument.Builder builder = new Instrument.Builder();
		Instrument instrument = builder.withCode(ApplicationHelper.LME_CODE).withDeliveryDate(ApplicationHelper.fromString("17-03-2018"))
				.withTradeDate(ApplicationHelper.fromString("15-03-2018")).withMarket("PB").withLabel("Lead 13 March 2018")
				.withSource(ApplicationHelper.EXCHANGE_LME).withAlternateId(ApplicationHelper.INSTRUMENT_CODE, "PB_03_2018").build();

		publisher.publish(instrument);

		logger.info("{}", processor.getLastInternallyPublishedInstrument());
	}

	private static void case2() {
		InstrumentProcessor processor = new InstrumentProcessor();

		ThreadSafePublisher exchangeLME = new ThreadSafePublisher(ApplicationHelper.EXCHANGE_LME);
		ThreadSafePublisher exchangePRIME = new ThreadSafePublisher(ApplicationHelper.EXCHANGE_PRIME);

		InstrumentListenerImpl listener = new InstrumentListenerImpl(processor);
		exchangeLME.registerListener(listener);
		exchangePRIME.registerListener(listener);

		Instrument lmeinstrument = new Instrument.Builder().withCode(ApplicationHelper.EXCHANGE_LME).withDeliveryDate(ApplicationHelper.fromString("17-03-2018"))
				.withTradeDate(ApplicationHelper.fromString("15-03-2018")).withMarket("PB").withLabel("Lead 13 March 2018")
				.withSource(ApplicationHelper.EXCHANGE_LME).withAlternateId(ApplicationHelper.LME_CODE, "PB_03_2018")
				.withAlternateId(ApplicationHelper.EXCHANGE_LME, "PB_03_2018").build();

		Instrument primeInstrument = new Instrument.Builder().withCode(ApplicationHelper.EXCHANGE_PRIME).withDeliveryDate(ApplicationHelper.fromString("17-03-2018"))
				.withTradeDate(ApplicationHelper.fromString("15-03-2018")).withMarket("LME_PB").withLabel("Lead 13 March 2018")
				.withSource(ApplicationHelper.EXCHANGE_PRIME).withAlternateId(ApplicationHelper.EXCHANGE_CODE, "PRIME_PB_03_2018")
				.tradable(false).build();

		exchangeLME.publish(lmeinstrument);
		logger.info("{}", processor.getLastInternallyPublishedInstrument());
		exchangePRIME.publish(primeInstrument);
		logger.info("{}", processor.getLastInternallyPublishedInstrument());

	}
	private static void case3() {
		InstrumentProcessor processor = new InstrumentProcessor();

		ThreadSafePublisher exchangeLME = new ThreadSafePublisher(ApplicationHelper.EXCHANGE_LME);
		ThreadSafePublisher exchangePRIME = new ThreadSafePublisher(ApplicationHelper.EXCHANGE_PRIME);

		InstrumentListenerImpl listener = new InstrumentListenerImpl(processor);
		exchangeLME.registerListener(listener);
		exchangePRIME.registerListener(listener);

		Instrument lmeinstrument = new Instrument.Builder().withCode(ApplicationHelper.EXCHANGE_LME).withDeliveryDate(ApplicationHelper.fromString("17-03-2018"))
				.withSource(ApplicationHelper.EXCHANGE_LME).withTradeDate(ApplicationHelper.fromString("15-03-2018"))
				.withMarket("LME_PB").withLabel("Lead 13 March 2018").withAlternateId(ApplicationHelper.LME_CODE, "PB_03_2018")
				.build();

		Instrument primeInstrument = new Instrument.Builder().withCode(ApplicationHelper.EXCHANGE_PRIME).withDeliveryDate(ApplicationHelper.fromString("18-03-2018"))
				.withTradeDate(ApplicationHelper.fromString("14-03-2018")).withMarket("LME_PB").withLabel("Lead 13 March 2018")
				.withAlternateId(ApplicationHelper.EXCHANGE_CODE, "PB_03_2018")
				.withAlternateId(ApplicationHelper.INSTRUMENT_CODE, "PRIME_PB_03_2018").tradable(false)
				.withSource(ApplicationHelper.EXCHANGE_PRIME).build();

		exchangePRIME.publish(primeInstrument);
		logger.info("{}", processor.getLastInternallyPublishedInstrument());
		exchangeLME.publish(lmeinstrument);
		logger.info("{}", processor.getLastInternallyPublishedInstrument());
	}
}
