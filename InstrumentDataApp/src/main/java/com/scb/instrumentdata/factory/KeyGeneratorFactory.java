package com.scb.instrumentdata.factory;

import java.util.Objects;

import com.scb.instrumentdata.model.Instrument;
import com.scb.instrumentdata.util.ApplicationHelper;

public class KeyGeneratorFactory implements ApplicationHelper {

	@FunctionalInterface
	public interface KeyGenerator {
		String getKey(Instrument instrument);
	}

	public static KeyGenerator generateForExchange(String source) {

		Objects.requireNonNull(source, "Source is required for Key generation");

		switch (source) {
		case EXCHANGE_LME:
			return (Instrument i) -> i.getAlternateId(LME_CODE);
		case EXCHANGE_PRIME:
			return (Instrument i) -> i.getAlternateId(EXCHANGE_CODE);
		default:
			return (Instrument i) -> i.getCode();
		}

	}

}
