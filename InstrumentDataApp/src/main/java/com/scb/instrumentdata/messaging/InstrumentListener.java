package com.scb.instrumentdata.messaging;
@FunctionalInterface
public interface InstrumentListener {

	public void onMessage(Message message);
	
}
