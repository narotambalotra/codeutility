package com.scb.instrumentdata.messaging;

import java.util.Objects;

import com.scb.instrumentdata.process.InstrumentProcessor;

public class InstrumentListenerImpl implements InstrumentListener {

	private InstrumentProcessor processor;

	public InstrumentListenerImpl(InstrumentProcessor processor) {
		this.processor = processor;
	}
   @Override
	public void onMessage(Message message) {

		Objects.requireNonNull(message, "Incoming message is null");
		Objects.requireNonNull(message.getSource(), "Incoming message's source is null");
		Objects.requireNonNull(message.getInstrument(), "Incoming message's instrument is null");

		processor.process(message);

	}

}
