package com.scb.instrumentdata.process;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.scb.instrumentdata.factory.InstrumentMergerFactory;
import com.scb.instrumentdata.factory.InstrumentMergerFactory.InstrumentMerger;
import com.scb.instrumentdata.messaging.Message;
import com.scb.instrumentdata.model.Instrument;

public class InstrumentProcessor {

	private Instrument lastInternallyPublishedInstrument;
	private Map<String, Instrument> messages = new ConcurrentHashMap<>();

	public void process(Message message) {
		Instrument arrivingInstrument = message.getInstrument();

		Instrument existingInstrument = messages.get(arrivingInstrument.getKey());

		InstrumentMerger instrumentMerger = InstrumentMergerFactory.generateForExchange(message.getSource());
		Instrument updatedInstrument = instrumentMerger.merge(existingInstrument, arrivingInstrument);

		publishInternally(updatedInstrument);

	}

	private void publishInternally(Instrument updatedInstrument) {

		// we can again publish this message to some listener queue, not doing
		// so for sake of no benefit for this test

		messages.put(updatedInstrument.getKey(), updatedInstrument);
		lastInternallyPublishedInstrument = updatedInstrument;
	}

	// for testing only
	public Instrument getLastInternallyPublishedInstrument() {
		return lastInternallyPublishedInstrument;
	}

}
