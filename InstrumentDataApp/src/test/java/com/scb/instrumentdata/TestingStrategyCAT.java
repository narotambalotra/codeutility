package com.scb.instrumentdata;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/referencedata.feature"
        ,glue={"com.scb.instrumentdata.steps"}
)
public class TestingStrategyCAT {

}
