package com.scb.instrumentdata.steps;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scb.instrumentdata.messaging.InstrumentListenerImpl;
import com.scb.instrumentdata.messaging.ThreadSafePublisher;
import com.scb.instrumentdata.model.Instrument;
import com.scb.instrumentdata.process.InstrumentProcessor;

public class Universe {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(InstrumentUpdaterSteps.class);

	private static Universe instance = null;
	private InstrumentProcessor processor = new InstrumentProcessor();
	private InstrumentListenerImpl listener = new InstrumentListenerImpl(
			processor);
	private Map<String, ThreadSafePublisher> publishers = new HashMap<>();
	private Map<String, Instrument> instrumentsToPublish = new HashMap<>();

	public static Universe getInstance() {
		if (instance == null) {
			instance = new Universe();
		}
		return instance;
	}

	public void addPublisher(String publisherSource) {
		ThreadSafePublisher publisher = new ThreadSafePublisher(publisherSource);
		publisher.registerListener(listener);
		publishers.put(publisherSource, publisher);
		LOGGER.info("Publisher added for Source {}", publisherSource);
	}

	public void publish(String source, String instrumentCode) {
		Instrument instrument = instrumentsToPublish.get(instrumentCode);
		publishers.get(source).publish(instrument);

	}

	public void addInstrument(Instrument instrument) {
		instrumentsToPublish.put(instrument.getCode(), instrument);
		LOGGER.info("Adding Instrument {}", instrument);
	}

	public Instrument getLastInternallyPublishedInstrument() {
		return processor.getLastInternallyPublishedInstrument();
	}

}
