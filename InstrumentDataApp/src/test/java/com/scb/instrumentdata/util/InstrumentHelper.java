package com.scb.instrumentdata.util;

import com.scb.instrumentdata.model.Instrument;
import com.scb.instrumentdata.util.ApplicationHelper;

public class InstrumentHelper {
	
	public static final String EMPTY_STRING = "";
	public static final String EXCHANGE_CODE_VALUE = "ExchangeCode";
	public static final String LME_CODE_VALUE = "LMECode";

	public static final Instrument LME_INSTRUMENT_WITH_LME_CODE = new Instrument.Builder()
			.withDeliveryDate(ApplicationHelper.fromString("14-03-2018"))
			.withSource(ApplicationHelper.EXCHANGE_PRIME)
			.withTradeDate(ApplicationHelper.fromString("15-03-2018"))
			.withMarket("LME_PB").withLabel("Lead 13 March 2018")
			.withAlternateId(ApplicationHelper.LME_CODE, LME_CODE_VALUE)
			.withCode("lme_code")
			.build();
	
	public static final Instrument LME_INSTRUMENT_WITHOUT_LME_CODE = new Instrument.Builder()
			.withDeliveryDate(ApplicationHelper.fromString("16-03-2018"))
			.withSource(ApplicationHelper.EXCHANGE_PRIME)
			.withTradeDate(ApplicationHelper.fromString("17-03-2018"))
			.withMarket("LME_PB").withLabel("Lead 13 March 2018")
			.withCode("lme_code")
			.build();
	
	public static final Instrument PRIME_INSTRUMENT_WITH_EXCHANGE_CODE = new Instrument.Builder()
			.withDeliveryDate(ApplicationHelper.fromString("18-03-2018"))
			.withSource(ApplicationHelper.EXCHANGE_PRIME)
			.withTradeDate(ApplicationHelper.fromString("15-03-2018"))
			.withMarket("LME_PB").withLabel("Lead 13 March 2018")
			.withAlternateId(ApplicationHelper.EXCHANGE_CODE, EXCHANGE_CODE_VALUE)
			.tradable(false)
			.withCode("prime_code")
			.build();
	
	public static final Instrument PRIME_INSTRUMENT_WITHOUT_EXCHANGE_CODE = new Instrument.Builder()
			.withDeliveryDate(ApplicationHelper.fromString("17-03-2018"))
			.withSource(ApplicationHelper.EXCHANGE_PRIME)
			.withTradeDate(ApplicationHelper.fromString("15-03-2018"))
			.withMarket("LME_PB").withLabel("Lead 13 March 2018")
			.withCode("prime_code")
			.build();

}
